/*
 MQTT Light sensor for the Nervadura project
*/

#include <ESP8266WiFi.h>
#include <PubSubClient.h>

// Use here your configuration file or fill configuration.h with propper values
#include "configuration_alternative.h"
#include "configuration.h"


#define MILLIS_TO_SEND_DEVICE_MSG 10000
#define MILLIS_TO_SAMPLE 10
#define MAX_MSG_SIZE 128
#define TRIGGER_THRESHOLD 100  // difference between current sample and last samples to be considered as a trigger
#define CLIENT_ID_SUFIX "sensor-"

// code to send in a sample message
enum sample_message_code {
  CODE_0 = 0,
  CODE_1 = 1,
  CODE_NO_CODE = 2
};

const char* topic_name_devices = "nervadura/devices";
const char* topic_name_sensor_triggered = "nervadura/sensor_triggered";
const char* topic_name_test_led = "nervadura/test_led";

int mqtt_sub_qos = 1;
String clientId;
String str_all; // When initialized at setup() will contain "ALL "
String str_ends_with_0;
String str_ends_with_1;

const int ldrPin = A0;

WiFiClient espClient;
PubSubClient client(espClient);

void setup_wifi() {
  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

// Returns the string sent to the devices topic. Currently, this is the device's client ID
const char * get_device_msg(){
  return clientId.c_str();
}

char sensor_triggered_event_msg_buff[MAX_MSG_SIZE];

char * get_sensor_triggered_event_msg(sample_message_code code){
  int i = 0;
  for (i; i < strlen(clientId.c_str()); i++){
    sensor_triggered_event_msg_buff[i] = clientId.c_str()[i];
    if (i == MAX_MSG_SIZE-1) {
      sensor_triggered_event_msg_buff[i] = 0;
      return sensor_triggered_event_msg_buff;
    }
  }
  sensor_triggered_event_msg_buff[i] = ' ';
  i = i + 1;
  if (code == CODE_0){
    sensor_triggered_event_msg_buff[i] = '0';
  } else {
    sensor_triggered_event_msg_buff[i] = '1';
  }
  i = i + 1;
  sensor_triggered_event_msg_buff[i] = 0;
  
  return sensor_triggered_event_msg_buff;
}


sample_message_code last_event_code_sent = CODE_NO_CODE; // this can be 0 or 1

#define PAST_SAMPLES_LEN 200
int past_samples_index = 0; // This is the first empty slot of the array
int past_samples[PAST_SAMPLES_LEN];

void add_to_sample_list(int sample) {
  past_samples[past_samples_index] = sample;
  past_samples_index = (past_samples_index + 1) % PAST_SAMPLES_LEN;
}

int get_last_samples_average() {
  int total = 0;
  for (int i = 0; i < PAST_SAMPLES_LEN; i ++) {
    total = total + past_samples[past_samples_index];
  }
  return total / PAST_SAMPLES_LEN;
}

sample_message_code get_code_from_sample(int new_sample) {
  sample_message_code res = CODE_NO_CODE;
  if (new_sample > get_last_samples_average() + TRIGGER_THRESHOLD) {
    if (last_event_code_sent != CODE_1) {
      res = CODE_1;
    }
  } else {
    if (last_event_code_sent == CODE_1) {
      res = CODE_0;
    }
  }
  add_to_sample_list(new_sample);
  return res;
}

void send_sensor_triggered_message(sample_message_code code){
  if (code == CODE_NO_CODE) {
    return;
  }
  Serial.print("Publish sample: ");
  Serial.println(get_sensor_triggered_event_msg(code));
  client.publish(topic_name_sensor_triggered, get_sensor_triggered_event_msg(code));
  last_event_code_sent = code;
}

bool this_message_addresses_me(String msg) {
  if (msg.startsWith(str_all)) {
    return true;
  }
  if (msg.startsWith(clientId)) {
    return true;
  }
  return false;
}

bool this_message_turns_me_on(String msg) {
  if (msg.endsWith(str_ends_with_1)) {
    return true;
  }
  return false;
}

bool this_message_turns_me_off(String msg) {
  if (msg.endsWith(str_ends_with_0)) {
    return true;
  }
  return false;
}

char payload_copy[MQTT_MAX_PACKET_SIZE + 1];

// Function called when a message arrives. The only topic the sensor is suscribed in is the test_led.
void callback(char* topic, byte* payload, unsigned int length) {
  for (uint i=0; i < length; i++){
    payload_copy[i] = (char) payload[i];
  }
  payload_copy[length] = 0;
  String msg = String(payload_copy);
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  Serial.println(msg.c_str());
  Serial.println(msg.length());
  
  if (this_message_addresses_me(msg)) {
    Serial.println("This message is for me!");
    if (this_message_turns_me_on(msg)) {
      Serial.println("Turning led on");
      digitalWrite(BUILTIN_LED, LOW);
    } else if (this_message_turns_me_off(msg)) {
      Serial.println("Turning led off");
      digitalWrite(BUILTIN_LED, HIGH);
    }
  }
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect(clientId.c_str())) {
      Serial.println("connected");
      client.publish(topic_name_devices, get_device_msg());
      client.subscribe(topic_name_test_led, mqtt_sub_qos);
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void setup() {
  pinMode(ldrPin, INPUT);
  uint32_t chipid;
  pinMode(BUILTIN_LED, OUTPUT);     // Initialize the BUILTIN_LED pin as an output
  chipid=ESP.getChipId();
  clientId = CLIENT_ID_SUFIX;
  clientId += String(chipid, HEX); // To access the client ID as a str use clientId.c_str()
  str_all = String("ALL ");
  str_ends_with_0 = String(" 0");
  str_ends_with_1 = String(" 1");
  Serial.begin(115200);
  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
}

long last_device_msg_sent_millis = 0;
long last_sample_millis = 0;

void loop() {

  if (!client.connected()) {
    reconnect();
  }
  client.loop();
  long now = millis();

  if (now - last_sample_millis > MILLIS_TO_SAMPLE) {
    last_sample_millis = now;
    int new_sample = analogRead(ldrPin);
    sample_message_code code = get_code_from_sample(new_sample);
    send_sensor_triggered_message(code);
  }
  
  if (now - last_device_msg_sent_millis > MILLIS_TO_SEND_DEVICE_MSG) {
    last_device_msg_sent_millis = now;
    Serial.print("Publish message: ");
    Serial.println(get_device_msg());
    client.publish(topic_name_devices, get_device_msg());
  }
}
